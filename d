
print('Функция-генератор')


def function(number):
    for counter in range(1, number + 1):
        yield counter ** 2


func_seq = function(10)
for counter in func_seq:
    print(counter, end=' ')

print()
print()
print('Генераторное выражение')
square_gen = (counter ** 2 for counter in range(1, 11))
for counter in square_gen:
    print(counter, end=' ')

print()
print()
print('Класс-итератор')


class Square_gen():
    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return self.counter ** 2
        else:
            raise StopIteration


square_gen = Square_gen(10)
for counter in square_gen:
    print(counter, end=' ')

# зачёт!
